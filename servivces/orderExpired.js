const Redis = require("ioredis")
let redis = new Redis()
function orderExpiredEvent({orderId,delay}){
    
    return redis.set(orderId,'Cancel Order Expired','EX',delay)
}

module.exports = {
    orderExpiredEvent
}