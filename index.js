const express = require('express');
const app = express();
const PORT = process.env.PORT || 8080
const session = require('express-session')
let RedisStore = require("connect-redis")(session)
const Redis = require("ioredis")
let redisClient = new Redis()
// so sánh giữa cookie vs session

// app.set('trust proxy', 1) // trust first proxy
app.use(session({
  secret: 'keyboard cat',
  store: new RedisStore({ client: redisClient }),
  resave: false,
  saveUninitialized: true,
  cookie: { 
    secure: false,
    httpOnly: true,
    maxAge: 5*60*1000

}
}))

app.get('/get-session', function(req, res){
    res.send(req.session)
})
app.get('/set-session', function(req, res){
   req.session.User = {
    username:"Tip javascript",
    age: 38,
    email:"nluat134@gmail.com"
   }
   res.send("ok")

})
app.listen(PORT,()=>{
    console.log('listening on port 8080:::');
})