const express =  require('express');
const { orderExpiredEvent } = require('./servivces/orderExpired');


const app = express();
app.use(express.json());

const PORT = process.env.PORT || 3001
app.post('/order', async function(req, res) {
    try {
        const { userId,order} = req.body;
        console.log(order);
        await orderExpiredEvent({
            orderId: order.id,
            delay:5
        })
    return res.json({
        status: 200,
        msg:order
    })
    } catch (error) {
     console.log(error);   
    }
})

app.listen(PORT,()=>{
    console.log('listening on port:::: '+PORT);
})