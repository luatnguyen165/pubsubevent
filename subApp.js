const express = require('express')
const app = express()

const Redis = require("ioredis")
const sub = new Redis();

sub.psubscribe('__keyevent@0__:expired');
sub.on('pmessage',(pattern, channel, message)=>{
    console.log('pmessage received::::',message);
} )

app.listen(30010,()=>{
    console.log('Subscription listening on port 30010');
})